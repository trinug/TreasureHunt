﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAwal : MonoBehaviour {

	public GameObject panelMenu, panelAwal;

	void Awake () {
		panelMenu.SetActive (false);
		panelAwal.SetActive (false);
		//		print ("balik " + PlayerPrefs.GetInt ("balik", 0));
		if (PlayerPrefs.GetInt ("balik", 0) == 1) {
			panelMenu.SetActive (true);
			panelAwal.SetActive (false);
			PlayerPrefs.SetInt ("balik", 0);
		} else {
			panelMenu.SetActive (false);
			panelAwal.SetActive (true);
		}
		Time.timeScale = 1;
	}
	// Update is called once per frame
	void Update () {
		
	}
}
