﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChannelingPoint : MonoBehaviour {

	public float channelingTime = 8f;
	private float currentTime;
	bool countBegin;
	Slider channelingSlider;
	// Use this for initialization
	void Start () {
		currentTime = 0;
		channelingSlider = transform.GetChild (0).GetChild (0).GetComponent<Slider> ();
	}

	// Update is called once per frame
	void Update () {
		if (countBegin) {
			currentTime += Time.deltaTime;
//			Debug.Log (currentTime);
			if (currentTime >= channelingTime)
				currentTime = 0;
			channelingSlider.value = currentTime / channelingTime;
		}

	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Gun Player") {
			//other.GetComponent<PlayerShooting> ().amoCount += ammoStored;
			countBegin = true;
			//Debug.Log ("a");
		}
	}
	void OnTriggerExit(Collider other){
		if (other.tag == "Gun Player") {
			//other.GetComponent<PlayerShooting> ().amoCount += ammoStored;
			countBegin = false;
			currentTime = 0;
			channelingSlider.value = 0;
			//Debug.Log ("ab");
		}
	}
}
