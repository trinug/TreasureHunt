﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorySelector : MonoBehaviour {

	public int currentLevel;
	public GameObject[] arenas;
	public GameObject[] narasies;
	// Use this for initialization
	void Start () {
		//commend default
		//PlayerPrefs.SetInt ("currentLevel", currentLevel);

		currentLevel = PlayerPrefs.GetInt ("currentLevel", 10)-10;
		Debug.Log (currentLevel);
		for (int i = 0; i < arenas.Length; i++) {
			arenas [i].SetActive (false);
			narasies [i].SetActive (false);
		}
		arenas [currentLevel].SetActive (true);
		narasies [currentLevel].SetActive (true);
		
	}
	
	// Update is called once per frame
	void Update () {
	}
}
