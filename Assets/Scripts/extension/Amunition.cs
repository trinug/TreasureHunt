﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amunition : MonoBehaviour {

	public int ammoStored;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//rotation
		transform.Rotate (new Vector3 (15, 45, 30) * Time.deltaTime);
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Gun Player") {
			PlayerShooting sht = other.GetComponent<PlayerShooting> ();
			sht.amoCount += ammoStored;
			if (sht.amoCount > sht.amoMax)
				sht.amoCount = sht.amoMax;
			Debug.Log (sht.amoCount);
			Destroy (gameObject);
		}
	}
}
