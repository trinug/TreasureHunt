﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void BackToHome(){
		ResumeGame ();
		PlayerPrefs.SetInt ("balik", 1);
		SceneManager.LoadScene (0);
	}

	public void QuitApps(){
		Application.Quit ();
	}

	public void LoadArena (int level){
		//stori level +10
		PlayerPrefs.SetInt ("currentLevel", level);
		if (level < 10)
			//SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
			SceneManager.LoadScene (1);
		else
			SceneManager.LoadScene (2);
	}

	public void RestartGame(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void PauseGame(){
		Time.timeScale = 0;
	}

	public void ResumeGame(){
		Time.timeScale = 1;
	}
}
