﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class DataManager : MonoBehaviour {

	public TextAsset soalXml;
	int jmlLvl, currentLevel;
	public string[] title, narasi;

	// Use this for initialization
	void Awake () {
		jmlLvl = 3;
		currentLevel = PlayerPrefs.GetInt ("currentLevel", 0);
		//LoadSoal ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadSoal(){
		title = new string[jmlLvl];
		narasi = new string[jmlLvl];
		int n = 0;
		XmlDocument xmlDoc = new XmlDocument(); 
		xmlDoc.LoadXml(soalXml.text);
//		int level = 0;
//		print (currentLevel);
		XmlNodeList rows = xmlDoc.FirstChild.SelectNodes("story")[0].SelectNodes("level");

		foreach (XmlNode row in rows) {
			//Debug.Log (row.Name+"\nbl"+row.InnerText+n);
			if (row.Name == "level") {
				//Debug.Log (row.InnerText);
				//print (row.SelectNodes("pertanyaan")[0].InnerText);
				title [n] = row.SelectNodes ("title") [0].InnerText;
				narasi [n] = row.SelectNodes ("narasi") [0].InnerText;
				//print (pertanyaan[n]);
				n++;
			}
		}
	}
}
