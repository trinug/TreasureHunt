﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public int currentLevel;
	public GameObject player;
	public GameObject[] arenas;
	public GameObject[] spawnPoints;
	//public GameObject[] arenaPoint;
	public int spawnPointNumber;
	// Use this for initialization
	void Awake () {
		currentLevel = PlayerPrefs.GetInt ("currentLevel", 0);
		Debug.Log (currentLevel);
		for (int i = 0; i < arenas.Length; i++) {
			arenas [i].SetActive (false);
		}
		for (int i = 0; i < spawnPoints.Length; i++) {
			spawnPoints [i].SetActive (true);
		}
		arenas [currentLevel].SetActive (true);
		player.transform.position = arenas [currentLevel].transform.Find ("arena point").position;
		for (int i = 0; i < spawnPointNumber; i++) {
			spawnPoints [i].SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
