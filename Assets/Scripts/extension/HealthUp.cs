﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUp : MonoBehaviour {

	public int healthStored;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		//rotations
		transform.Rotate (new Vector3 (15, 45, 30) * Time.deltaTime*2);
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			PlayerHealth hlt = other.GetComponent<PlayerHealth> ();
			hlt.currentHealth += healthStored;
			if (hlt.currentHealth > hlt.startingHealth)
				hlt.currentHealth = hlt.startingHealth;
			hlt.healthSlider.value=hlt.currentHealth;
			Debug.Log (hlt.currentHealth);
			Destroy (gameObject);
		}
	}
}
