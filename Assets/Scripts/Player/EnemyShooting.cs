﻿using UnityEngine;

public class EnemyShooting : MonoBehaviour{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;

	GameObject target;
	PlayerHealth playerHealth;

    float timer, shootTimer;
    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.05f;


    void Awake (){
		shootTimer = timeBetweenBullets;
		timeBetweenBullets = shootTimer + Random.Range (-0.5f, 1f);
		shootableMask = LayerMask.GetMask ("Shootable");
		gunParticles = GetComponent<ParticleSystem> ();
		gunLine = GetComponent <LineRenderer> ();
		gunAudio = GetComponent<AudioSource> ();
		gunLight = GetComponent<Light> ();
		//player = GameObject.FindGameObjectWithTag ("Player");
		//playerHealth = player.GetComponent <PlayerHealth> ();
	}


    void Update (){
		timer += Time.deltaTime;

        if(timer >= timeBetweenBullets){
						timeBetweenBullets = shootTimer + Random.Range (-0.5f, 1f);
            Shoot ();
						timer = 0f;
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime){
            DisableEffects ();
        }
    }


    public void DisableEffects (){
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
	{
//		Debug.Log (shootRay);
		timer = 0f;

		gunAudio.Play ();

		gunLight.enabled = true;

		gunParticles.Stop ();
		gunParticles.Play ();

		gunLine.enabled = true;
		gunLine.SetPosition (0, transform.position);

		shootRay.origin = transform.position;
		shootRay.direction = transform.forward;

		if (Physics.Raycast (shootRay, out shootHit, range, shootableMask)) {
			//EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
//			Debug.Log(shootHit.collider.gameObject);
			target = shootHit.collider.gameObject;
			playerHealth = target.GetComponent <PlayerHealth> ();
			if (playerHealth != null && target.tag == "Player") {
				playerHealth.TakeDamage (damagePerShot);
//								print (player);
			}
			gunLine.SetPosition (1, shootHit.point);
		} else {
			gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
		}
	}
}
