﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
	public static int score;
	//public static int highScore;
	//public Text scoreText, highScoreText;
	public Text scoreText;
	public QuestManager questManager;

	//Text text;
	//string highScoreKey = "highScore";

    void Awake ()
    {
//		Debug.Log (gameObject.name);
		//highScore = PlayerPrefs.GetInt (highScoreKey, 0);
		//text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
		scoreText.text = score + " / " + questManager.enemyToDefeat;
		/*
		if (highScore <= score) {
			highScore = score;
		}
		*/
		//highScoreText.text = "High Score : " + highScore;
    }
}
