﻿using System.Collections;
using UnityEngine;

public class EnemyManager : MonoBehaviour{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

	QuestManager manager;

    void Start () {
		StartCoroutine (StartSpawning ());
		//Debug.Log (transform.parent.gameObject.activeSelf);
		manager = GameObject.Find ("Game Manager").GetComponent<QuestManager> ();
    }

	IEnumerator StartSpawning(){
		//Debug.Log ("start");
		yield return null;
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
		//Debug.Log ("invoke");
	}

    void Spawn () {
		if (playerHealth.currentHealth <= 0f || manager.enemySpawned >= manager.enemyToDefeat) {
			return;
		}

		int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		Instantiate (enemy, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);
		//Debug.Log (spawnPoints[spawnPointIndex]);
		manager.enemySpawned++;
	}
}
