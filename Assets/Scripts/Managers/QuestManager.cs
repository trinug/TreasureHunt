﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour {

	// Use this for initialization
	public Toggle[] questChecklist;

	public bool allWaveCleared, pointReached;

	public int[] enemyPerLevel;
	public int enemyToDefeat, enemySpawned, enemyDefeated;

	public GameObject[] dropItems;

	public GameObject panelAwal;

	GameOverManager overManager;
	ButtonController menuControl;
	DataManager storyData;

	void Awake () {
		int currentLevel = PlayerPrefs.GetInt ("currentLevel", 0);
		enemyToDefeat = enemyPerLevel [currentLevel];
		for (int i = 0; i < questChecklist.Length; i++) {
			questChecklist [i].isOn = false;
		}
		overManager = GameObject.Find ("GameOver").GetComponent<GameOverManager> ();
		storyData = GameObject.Find ("GameOver").GetComponent<DataManager> ();
		menuControl = GameObject.Find ("Button Controller").GetComponent<ButtonController> ();
		panelAwal.SetActive (true);
		menuControl.PauseGame ();
		storyData.LoadSoal ();
		panelAwal.transform.GetChild (0).GetComponent<Text> ().text = storyData.title [currentLevel];
		panelAwal.transform.GetChild (1).GetComponent<Text> ().text = storyData.narasi [currentLevel];
	}
	
	// Update is called once per frame
	void Update () {
		if (allWaveCleared && questChecklist [0].isOn == false) {
			questChecklist [0].isOn = true;
			overManager.GameOver ();
		}
		if (pointReached && questChecklist [1].isOn == false) {
			questChecklist [1].isOn = true;
		}
	}

	public void DropItem(Vector3 posItem){
		int itemIndex = Random.Range (0, dropItems.Length);
		//Debug.Log (posItem);
		Vector3 adjustmentPos = new Vector3 (posItem.x, 0.4f, posItem.z);
		Instantiate (dropItems [itemIndex], adjustmentPos, transform.rotation);
	}
}
