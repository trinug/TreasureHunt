﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour{
    public PlayerHealth playerHealth;
	public float restartDelay = 5f;	
	public GameObject panelGameOver;
	// + highscore manager
    Animator anim;
	float restartTimer;	
	int highScore;	
	string highScoreKey = "highScore";

	void Start (){
		panelGameOver.SetActive (false);
		Initialize ();
	}

    void Awake(){
        anim = GetComponent<Animator>();
    }


    void Update(){

		if (playerHealth.currentHealth <= 0) {
			GameOver ();
		}
	}

	private void Initialize (){
		highScore = PlayerPrefs.GetInt (highScoreKey, 0);
	}
	
	public void GameOver(){
		panelGameOver.SetActive (true);
//		anim.SetTrigger ("GameOver");
//		highScore = ScoreManager.highScore;

		Save ();

		restartTimer += Time.deltaTime;

		if (restartTimer >= restartDelay) {
			//Application.LoadLevel (Application.loadedLevel);
			Initialize ();
		}
	}
	public void Save (){
		//Save the highscore to the player prefs
		PlayerPrefs.SetInt (highScoreKey, highScore);
		PlayerPrefs.Save ();
		//Re initialize the score
		Initialize ();
	}
}
