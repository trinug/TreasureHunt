﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public float smooth = 5;

	public Vector3 ofset;

	void Start(){
		Vector3 adjustment = new Vector3 (target.position.x, target.position.y + 14.5f, target.position.z - 24f);
		transform.position = adjustment;
		ofset = transform.position - target.position;
//		Debug.Log (adjustment);
	}

	void FixedUpdate(){
		Vector3 targetCamPos = ofset + target.position;
		transform.position = Vector3.Lerp (transform.position, targetCamPos, Time.deltaTime);
	}
}
